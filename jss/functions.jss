function randColor(a) {
    return "#" + (a * 10);
}

function createBlock(w, h, p, m) {
    return "width: " + w + "px;\
            height: " + h + "px;\
            padding: " + p + "px;\
            margin: " + m + "px;";
}