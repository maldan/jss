@import "jss/vars.jss";
@import "jss/functions.jss";

.block {
    background: #ff0000;

    <- createBlock(100, 100, 5, 10);

    [@screen < e(screen.large)] {
        <- createBlock(50, 50, 5, 10);
    }

    [@screen < e(screen.middle)] {
        <- createBlock(25, 25, 5, 10);
    }
}