var fs = require('fs');
var contents = fs.readFileSync('jss/main.jss').toString();

var VM = require("./modules/VirtualMachine").VM;
var PARSER = require("./modules/Parser").parser;
var BUILTIN = require("./modules/Builtin").builtin;
var FS = require("./modules/FileManager").fileManager;
var RVM = require('vm');

VM.vars = PARSER.findVars(contents);
PARSER.builtin = BUILTIN;
PARSER.code = contents;
PARSER.fileManager = FS;
PARSER.rvm = RVM;

while (PARSER.parseImports()) {};

PARSER.parseVars();
PARSER.parseFunctions();
PARSER.parseRepeats();
PARSER.parseEvaluations();
PARSER.parseEchoFunctions();
PARSER.findNestedBlocks(PARSER.code);

var data = PARSER.generateAllCss() + PARSER.generateMediaQueries();
FS.save("style.css", data);