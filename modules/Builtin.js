var Builtin = function() {};

Builtin.prototype.w2 = function(x, y)
{
    return (x * y) + "px";
};

Builtin.prototype.darken = function(color, percent)
{
    var rgb = {};
    var v = (percent.replace("%", "") * 1) / 100;
    rgb.r = (((color.replace("#", "0x") * 1) & 0xFF0000) >> 16);
    rgb.g = (((color.replace("#", "0x") * 1) & 0x00FF00) >> 8);
    rgb.b = (((color.replace("#", "0x") * 1) & 0x0000FF));

    var new_rgb = { r: 0, g: 0, b: 0 };

    new_rgb = { r: rgb.r * ((1 - v)), g: rgb.g * ((1 - v)), b: rgb.b * ((1 - v)) };

    return this.rgbToHEX(new_rgb);
};

Builtin.prototype.lighten = function(color, percent)
{
    var rgb = {};
    var v = (percent.replace("%", "") * 1) / 100;
    rgb.r = (((color.replace("#", "0x") * 1) & 0xFF0000) >> 16);
    rgb.g = (((color.replace("#", "0x") * 1) & 0x00FF00) >> 8);
    rgb.b = (((color.replace("#", "0x") * 1) & 0x0000FF));

    var new_rgb = { r: 0, g: 0, b: 0 };

    if (v < 0) new_rgb = { r: 255 - ((255 - rgb.r) * v), g: 255 - ((255 - rgb.g) * v) , b: 255 - ((255 - rgb.b) * v) };
    else new_rgb = { r: rgb.r * (1 - (1 - v)), g: rgb.g * (1 - (1 - v)), b: rgb.b * (1 - (1 - v)) };

    return this.rgbToHEX(new_rgb);
};

Builtin.prototype.rgbToHEX = function(color)
{
    var R = ( "0" + (~~color.r).toString(16).toUpperCase()).slice(-2);
    var G = ( "0" + (~~color.g).toString(16).toUpperCase()).slice(-2);
    var B = ( "0" + (~~color.b).toString(16).toUpperCase()).slice(-2);

    return "#" + R + G + B;
};

Builtin.prototype.hsvToRGB = function(color)
{
    var C = (color.v * color.s);

    var X = C * (1 - Math.abs((color.h / 60) % 2 - 1));
    var m = color.v - C;
    var rgb = [0, 0, 0];

    if (color.h >= 0 && color.h < 60) rgb = [C, X, 0];
    if (color.h >= 60 && color.h < 120) rgb = [X, C, 0];
    if (color.h >= 120 && color.h < 180) rgb = [0, C, X];
    if (color.h >= 180 && color.h < 240) rgb = [0, X, C];
    if (color.h >= 240 && color.h < 300) rgb = [X, 0, C];
    if (color.h >= 300 && color.h < 360) rgb = [C, 0, X];

    var r = { r: ((rgb[0] + m) * 255) | 0, g: ((rgb[1] + m) * 255) | 0, b: ((rgb[2] + m) * 255) | 0 };

    if (r.r > 255) r.r = 255;
    if (r.g > 255) r.g = 255;
    if (r.b > 255) r.b = 255;

    return r;
};

Builtin.prototype.rgbToHSV = function(color)
{
    var r = color.r / 255;
    var g = color.g / 255;
    var b = color.b / 255;

    var cmin = Math.min(r, g, b);
    var cmax = Math.max(r, g, b);

    var delta = cmax - cmin;

    var h = 0, s = 0, v = 0;

    // Hue calculate
    if (delta == 0) h = 0;
    else if (r == cmax) h = (g - b) / delta;
    else if (g == cmax) h = 2 + (b - r) / delta;
    else if (b == cmax) h = 4 + (r - g) / delta;
    h *= 60;
    if (h < 0) h += 360;

    // Saturation calculate
    if (cmax == 0) s = 0;
    if (cmax != 0) s = delta / cmax;

    // Value
    v = cmax;

    return {h: h, s: s, v: v};
};

exports.builtin = new Builtin();