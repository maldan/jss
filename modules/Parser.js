var Parser = function() {};

Parser.prototype.mediaQueries = {};
Parser.prototype.allSelectors = [];
Parser.prototype.builtin = null;
Parser.prototype.code = "";
Parser.prototype.rvm = null;
Parser.prototype.fileManager = null;

Parser.prototype.pushMediaQuery = function(code, stack, condition) {
    var tmp1 = code.split(";");
    var selectors = {};
    for (var i = 0; i < tmp1.length; i++) {
        var tmp2 = tmp1[i].split(":");
        if (!tmp2[0]) continue;
        selectors[tmp2[0]] = tmp2[1];
    }

    if (!this.mediaQueries[condition]) {
        this.mediaQueries[condition] = {
            data: [{
                stack: [].concat(stack),
                selectors: selectors
            }],
            value: condition.replace(/<|>/g, "") * 1
        };
    } else {
        this.mediaQueries[condition].data.push({
            stack: [].concat(stack),
            selectors: selectors
        });
    }
};

Parser.prototype.parseEchoFunctions = function()
{
    var self = this;

    this.code = this.code.replace(/<\-\s+?(.*?);/g, function(match, $params)
    {
        self.rvm.createContext(self.builtin);
        return self.rvm.runInContext($params, self.builtin);
    });
};

Parser.prototype.parseImports = function()
{
    var self = this;
    var replaced = false;

    this.code = this.code.replace(/@import "(.*?)";/g, function(match, $params)
    {
        replaced = true;
        return self.fileManager.load($params);
    });

    return replaced;
};

// Parse `e` functions
Parser.prototype.parseEvaluations = function()
{
    var self = this;

    this.code = this.code.replace(/e\((.*?)\)/g, function(match, $params)
    {
        self.rvm.createContext(self.builtin);
        return self.rvm.runInContext($params, self.builtin);
    });
};

// Parse and handle `repeat` literal
Parser.prototype.parseRepeats = function()
{
    var repeat_regex = /<repeat (-?[0-9]+):(-?[0-9]+):(-?[0-9]+)>((\s|.)*?)<\/repeat>/g;
    var repeat_data = repeat_regex.exec(this.code);
    var tmp_buffer, start = 0, final_buffer = "";

    // If repeat was found
    if (repeat_data) {
        var from = repeat_data[1] * 1;
        var to = repeat_data[2] * 1;
        var step = repeat_data[3] * 1;
        var code = repeat_data[4];

        start = repeat_data.index;

        for (var i = from; i < to; i += step) {
            tmp_buffer = code;
            tmp_buffer = tmp_buffer.replace(/\$_/g, i);
            final_buffer += tmp_buffer;
        }

        var start_code = this.code.substring(0, start);
        var end_code = this.code.substring(start += repeat_data[0].length, this.code.length);

        this.code = start_code + final_buffer + end_code;
    }
};

Parser.prototype.parseVars = function()
{
    var var_regex = /var ([a-zA-Z0-9_]+)\s+?=\s+?((\s|.)*?);/g;
    var var_data = var_regex.exec(this.code);

    if (var_data)
    {
        this.rvm.createContext(this.builtin);
        this.rvm.runInContext(var_data[0], this.builtin);

        var start_code = this.code.substring(0, var_data.index);
        var end_code = this.code.substring(var_data.index + var_data[0].length, this.code.length);

        this.code = start_code + end_code;

        this.parseVars();
    }
};

Parser.prototype.parseFunctions = function()
{
    var function_regex = /function ([a-zA-Z0-9]+)\((.*?)?\)\s+?{/g;
    var function_data = function_regex.exec(this.code);
    var i = 0;
    var start = 0;
    var scopes = 0;
    var code = "";
    var buffer = "";

    // If literal of function has been found
    if (function_data) {
        start = function_data.index;
        // Start code position
        i = start + function_data[0].length + 1;
        scopes = 1;
    }

    // Parse function until its close
    while (scopes > 0) {
        code = this.code[i];
        buffer += code;

        if (code == "{") scopes += 1;
        if (code == "}") scopes -= 1;

        i++;
    }

    // Normalize buffer
    buffer = buffer.substr(0, buffer.length - 1);

    // If literal of function has been found, create new function
    // Put function into Builtin class
    if (function_data) {
        var function_name = function_data[1];
        var function_params = function_data[2] || "";
        this.builtin[function_name] = new Function(function_params.split(","), buffer);

        console.log("Create function " + function_name);
    }

    // Erase function code
    var start_code = this.code.substring(0, start);
    var end_code = this.code.substring(i, this.code.length);
    this.code = start_code + end_code;

    // Make recursive
    if (function_data && function_data.index)
        this.parseFunctions();
};

Parser.prototype.findNestedBlocks = function(code, prnt) {
    var selector_regex = /\.?[a-zA-Z0-9>\-:\[\]]+\s+?{/gm; //selector regex

    var scope = 0;
    var selector_position = selector_regex.exec(code);
    var selector_position_nested = "";
    var i = 0, start = 0, end = 0, selector_name = "", selector_name_nested = "";
    var selectors = {};
    var stack = [];

    if (prnt) for (var ii = 0; ii < prnt.length; ii++) stack.push(prnt[ii]);

    var buffer = "", startNested = false, nestedCodeBuffer = "";

    // If found selector
    if (selector_position) {
        scope = 1;
        start = i = selector_position.index + selector_position[0].length;
        selector_name = selector_position[0].substr(0, selector_position[0].indexOf("{")).replace(/\s+/, "");
    } else {
        return;
    }

    // Replace fast nth-child pseudo selectors
    selector_name = selector_name.replace(/([a-zA-Z]+)\[(\-?[0-9]+)]/, function(match, $1, $2) {
        var position = ":first-child";
        if ($2 > 0) position = ":nth-child(" + ($2 * 1 + 1) + ")";
        if ($2 == -1) position = ":last-child";
        return $1 + position;
    });

    // Update selectors
    selectors = { name: selector_name, stack: stack };

    // Loop code while cursor reached last }
    while (scope > 0)
    {
        if (code[i] == "{") scope += 1;
        if (code[i] == "}") scope -= 1;

        if (code[i] == "{" && scope == 2) {
            startNested = true;
            selector_name_nested = "";

            for (var j = 0; j < 256; j++) {
                if (code[i - 1 - j].match(/(;|}|{)/g)) break;
                selector_name_nested = code[i - 1 - j] + selector_name_nested;
            }

            selector_name_nested = selector_name_nested.replace(/\s+/g, '');

            stack.push(selector_name);
            nestedCodeBuffer += selector_name_nested + " {";
        }

        if (code[i] == "}" && scope == 1) startNested = false;

        i++;
        end = i;

        // If we in main selector block
        if (scope == 1) buffer += code[i];

        if (startNested) nestedCodeBuffer += code[i];
        if (!startNested && nestedCodeBuffer != "")
        {
            if (nestedCodeBuffer.match(/\[@screen([<])([0-9]+)]/))
            {
                nestedCodeBuffer = nestedCodeBuffer.replace(/(\s+|\t+|\n+|\r+)/g, "");
                var mqTmp = nestedCodeBuffer.match(/\[@screen([<])([0-9]+)]{(.*?)}/);

                if (mqTmp) {
                    this.pushMediaQuery(mqTmp[3], stack, mqTmp[1] + "" + mqTmp[2]);
                }
            } else {
                this.findNestedBlocks(nestedCodeBuffer, stack);
            }

            stack.pop();
            nestedCodeBuffer = "";
        }

        // If css property is end
        if (code[i] == ";")
        {
            // Check if it is css property
            var css_property = buffer.match(/([a-zA-Z0-9\-]+):\s+?(.*?);/);
            if (css_property) {
                //selectors[selector_name][css_property[1]] = css_property[2];

                var tmp_match = css_property[2].match(/([a-zA-Z0-9]+)\((.*?)?\)/);

                if (tmp_match)
                {
                    var tmp_params = [];

                    if (tmp_match[2]) {
                        tmp_params = tmp_match[2].split(",");
                    }

                    if (!this.builtin[tmp_match[1]]) {
                        throw new Error("Builtin function " + tmp_match[1] + " not found!");
                    }

                    try {
                        selectors[css_property[1]] = this.builtin[tmp_match[1]].apply(this.builtin, tmp_params);
                    }
                    catch (e)
                    {
                        console.log(e);
                    }
                } else {
                    selectors[css_property[1]] = css_property[2];
                }
            }
            buffer = "";
        }
    }

    //console.log(selectors);

    // Get final block code
    var nextCode = code.substr(end + 1, code.length);

    this.allSelectors.push(selectors);
    //console.log(this.generateCss(selectors));
    //console.log(selectors);

    this.findNestedBlocks(nextCode);
};

Parser.prototype.generateMediaQueries = function() {
    var CSS = "";
    var QS = [];

    for (var sin in this.mediaQueries) {
        QS.push(this.mediaQueries[sin]);
    }

    QS.sort(function(a, b) {
        return b.value - a.value;
    });

    for (var i = 0; i < QS.length; i++) {
        CSS += "@media (max-width: " + QS[i].value + "px) {\n";
        
        for (var di in QS[i].data)
        {
            CSS += "\t" + QS[i].data[di].stack.join(" ") + " { \n";

            for (var prop in QS[i].data[di].selectors) {

                var tmp_prop = QS[i].data[di].selectors[prop];
                var tmp_match = QS[i].data[di].selectors[prop].match(/([a-zA-Z0-9]+)\((.*?)?\)/);

                if (tmp_match)
                {
                    var tmp_params = [];

                    if (tmp_match[2])
                        tmp_params = tmp_match[2].split(",");

                    if (!this.builtin[tmp_match[1]]) {
                        throw new Error("Builtin function " + tmp_match[1] + " not found!");
                    }

                    try {
                        tmp_prop = this.builtin[tmp_match[1]].apply(this.builtin, tmp_params);
                    }
                    catch (e)
                    {
                        console.log(e);
                    }
                }

                CSS += "\t\t" + prop + ": " + tmp_prop + ";\n";

            }

            CSS += "\t}\n";
        }

        CSS += "}\n";
    }

    return CSS;
};

Parser.prototype.generateAllCss = function() {
    var CSS = "";

    for (var i = 0; i < this.allSelectors.length; i++)
        CSS += this.generateCss(this.allSelectors[i]);

    return CSS;
};

Parser.prototype.generateCss = function(selectors) {
    var css = "";
    var stack = "";
    var count_property = 0;

    // make stack
    for (var i = 0; i < selectors["stack"].length; i++) {
        stack += selectors["stack"][i] + " ";
    }

    // count properties
    for (var cnt in selectors) count_property += 1;

    if (count_property == 2) return "";

    // generate selectors
    for (var s in selectors)
    {
        if (s == "stack") continue;
        if (s == "name") { css += stack + selectors[s] + " {\n"; continue; }

        css += "\t" + s + ": " + selectors[s] + ";\n";
    }

    css += "}\n";

    return css;
};

Parser.prototype.findVars = function(code) {
    var reg = /var ([a-zA-Z_]+) = (.*?);/g;
    var vars = {}, readVars;

    while (readVars = reg.exec(code))
    {
        vars[readVars[1]] = readVars[2];
    }

    return vars;
};

exports.parser = new Parser();