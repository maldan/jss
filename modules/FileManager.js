var FileManager = function () {};

FileManager.prototype.save = function(filename, data) {
    var fs = require('fs');

    fs.writeFile(filename, data, function (err) {
        if (err)
            return console.log(err);
    });
};

FileManager.prototype.load = function(filename) {
    var fs = require('fs');

    return fs.readFileSync(filename).toString();
};

exports.fileManager = new FileManager();